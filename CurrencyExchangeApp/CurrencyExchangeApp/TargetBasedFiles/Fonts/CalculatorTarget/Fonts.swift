//
//  Fonts.swift
//  CurrencyExchangeApp
//
//  Created by Shrirang C. Kawade on 25/03/22.
//

import Foundation


enum Fonts: String {
    case light = "Apple SD Gothic Neo Light"
    case medium = "Apple SD Gothic Neo Medium"
    case regular = "Apple SD Gothic Neo Regular"
    case bold = "Apple SD Gothic Neo ExtraBold"
    case semiBold = "Apple SD Gothic Neo SemiBold"
}
