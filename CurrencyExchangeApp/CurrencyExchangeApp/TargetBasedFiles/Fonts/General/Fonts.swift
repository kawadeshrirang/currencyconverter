//
//  Fonts.swift
//  CurrencyExchangeApp
//
//  Created by Shrirang C. Kawade on 25/03/22.
//

import Foundation


enum Fonts: String {
    case light = "Avenir Light"
    case medium = "Avenir Medium"
    case regular = "Avenir Book"
    case bold = "Avenir Heavy"
    case semiBold = "Avenir Heavy Oblique"
}
