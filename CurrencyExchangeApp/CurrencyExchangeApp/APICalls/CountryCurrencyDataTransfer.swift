//
//  CountryCurrencyDataTransfer.swift
//  CurrencyExchangeApp
//
//  Created by Shrirang C. Kawade on 23/03/22.
//

import Foundation
import NetworkCallLibrary

class CountryCurrencyDataTransfer: BaseDataTransfer {
    
    // MARK: Properties
    private var webServiceURL: String = "https://www.westpac.com.au/bin/getJsonRates.wbc.fx.json"
    
    // MARK: LifeCycle Method
    init() {
        super.init(_baseURL: webServiceURL)
    }
    
    override func parseResposeData(data: Any?) {
        
        DispatchQueue.global(qos: .userInteractive).async {
            var currenyDataList: [CountryCurrency] = []
            
            if let data = data as? [String : Any],
               let allData = data["data"] as? [String : Any],
               let brands = allData["Brands"] as? [String : Any],
               let wbc = brands["WBC"] as? [String : Any],
               let portfolios = wbc["Portfolios"] as? [String : Any],
               let fx = portfolios["FX"] as? [String : Any],
               let products = fx["Products"] as? [String : Any] {
                
                for countryKey in products.keys {
                    
                    if let countryData = products[countryKey] as? [String : Any],
                       let currencyRates = countryData["Rates"] as? [String : Any],
                       let currenyData = currencyRates[countryKey] as? [String : Any] {
                        
                        let countryCurrency: CountryCurrency = CountryCurrency(data: currenyData)
                        currenyDataList.append(countryCurrency)
                    }
                }
                currenyDataList = currenyDataList.sorted { $0.country.localizedCaseInsensitiveCompare($1.country) == ComparisonResult.orderedAscending }
            }
            DispatchQueue.main.async { [weak self] in
                self?.dataTransferDelegate?.didRecieveResponse(dataTransfer: self, data: currenyDataList)
            }
        }
    }
    
    override func parseErrorData(error: NSError?) {
        super.parseErrorData(error: error)
    }
    
    // MARK: Public Method
    func getCountryList() {
        callWebService(ofType: .get)
    }
}
