//
//  FontManager.swift
//  CurrencyExchangeApp
//
//  Created by Shrirang C. Kawade on 25/03/22.
//

import Foundation
import UIKit

class FontManager {
    
    // MARK: LifeCycle Method
    private init() {
    }
    
    // MARK: Public Method
    public static func font(fontName: Fonts, fontSize: CGFloat) -> UIFont {
        let font = UIFont(name: fontName.rawValue, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        return font
    }
}
