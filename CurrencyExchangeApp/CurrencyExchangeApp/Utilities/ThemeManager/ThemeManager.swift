//
//  ThemeManager.swift
//  CurrencyExchangeApp
//
//  Created by Shrirang C. Kawade on 24/03/22.
//

import Foundation
import UIKit

extension UIColor {
    
    // MARK: Properties
    public static var viewBackgroundColor: UIColor {
        get {
            return UIColor.init(named: "BackgroundColor") ?? UIColor.clear
        }
    }
    
    public static var textColor: UIColor {
        get {
            return UIColor.init(named: "TextColour") ?? UIColor.clear
        }
    }
    
    public static var cellBackgroundColor: UIColor {
        get {
            return UIColor.init(named: "CellBackgroundColor") ?? UIColor.clear
        }
    }
    
    public static var buttonColor: UIColor {
        get {
            return UIColor.init(named: "ButtonColor") ?? UIColor.clear
        }
    }
}
