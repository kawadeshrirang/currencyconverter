//
//  ScreenManager.swift
//  CurrencyExchangeApp
//
//  Created by Shrirang C. Kawade on 23/03/22.
//

import Foundation
import UIKit

class ScreenManager: NSObject {
    
    // MARK: Properties
    static let shared: ScreenManager = ScreenManager()
    
    // MARK: LifeCycle Method
    private override init() {
        // made private initialisation of class.
    }
    
    // MARK: Private Methods
    private func getProjectExtension() -> String {
        let extensionName: String = Bundle.main.infoDictionary!["CFBundleName"] as! String
        return extensionName + "."
    }
    
    private func replaceProjectExtension(clasName: String) -> String {
        return clasName.replacingOccurrences(of: getProjectExtension(), with: "")
    }
    
    private func sceneDelegate() -> SceneDelegate? {
        return UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate
    }
    
    private func rootViewController() -> UIViewController? {
        let delegate = self.sceneDelegate()
        return delegate?.window?.rootViewController
    }
    
    private func getControllerName(controller: AnyClass) -> String {
        var controllerName = NSStringFromClass(controller)
        controllerName = replaceProjectExtension(clasName: controllerName)
        return controllerName
    }
    
    private func viewOfClassName(anyClass: AnyClass) -> String {
        return String(describing: anyClass).components(separatedBy: ".").last!
    }
    
    // MARK: Public Methods
    func xibForClass(className: AnyClass, owner: Any?) -> UINib {
        let xib = UINib(nibName: viewOfClassName(anyClass: className), bundle: Bundle.main)
        return xib
    }
    
    func viewForClass(className: AnyClass, owner: Any?) -> UIView {
        let xib = xibForClass(className: className, owner: owner)
        let view = xib.instantiate(withOwner: owner, options: nil).first as! UIView
        return view
    }
    
    func getViewController(controller: AnyClass) -> UIViewController {
        var controllerName = NSStringFromClass(controller)
        let viewController = NSClassFromString(controllerName) as! UIViewController.Type
        controllerName = replaceProjectExtension(clasName: controllerName)
        let instanceController = viewController.init(nibName: controllerName, bundle: Bundle.main)
        return instanceController
    }
    
    func launchHomeScreen() {
        let delegate = self.sceneDelegate()
        delegate?.window?.backgroundColor = UIColor.red
        delegate?.window?.makeKeyAndVisible()
        let controller = getViewController(controller: CountryListingController.self)
        let navigation = UINavigationController(rootViewController: controller)
        navigation.navigationItem.hidesBackButton = true
        delegate?.window?.rootViewController = navigation
    }
    
    func pushController(of controller: UIViewController, navigationController: UINavigationController?) {
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func pushController(of className: AnyClass, navigationController: UINavigationController?) {
        let controller = getViewController(controller: className)
        navigationController?.pushViewController(controller, animated: true)
    }
}
