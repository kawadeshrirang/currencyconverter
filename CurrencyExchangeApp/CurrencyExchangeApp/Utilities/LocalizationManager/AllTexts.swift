//
//  AllTexts.swift
//  CurrencyExchangeApp
//
//  Created by Shrirang C. Kawade on 24/03/22.
//

import Foundation

enum AllTextsStrings: String {
    
    case countryListTitle = "Country List"
    case currencyExchange = "Currency Exchange"
    case currencyAmount = "Currency Amount"
    case australia = "Australia"
    
    // MARK: Properties
    var localizedString: String {
        let fileName: String = "AllTexts"
        let keyValue: String = self.rawValue
        let finalText = NSLocalizedString(keyValue, tableName: fileName, bundle: Bundle.main, value: keyValue, comment: keyValue)
        return finalText
    }
}
