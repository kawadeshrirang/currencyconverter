//
//  ThemeLabel.swift
//  CurrencyExchangeApp
//
//  Created by Shrirang C. Kawade on 25/03/22.
//

import UIKit

class ThemeLabel: UILabel {
    
    // MARK: Public Method
    func initializeView(titleText: String) {
        text = titleText
        font = FontManager.font(fontName: Fonts.semiBold, fontSize: font.pointSize)
        textColor = UIColor.textColor
    }
}
