//
//  ThemeTextField.swift
//  CurrencyExchangeApp
//
//  Created by Shrirang C. Kawade on 25/03/22.
//

import UIKit

class ThemeTextField: UITextField {
    
    // MARK: Public Method
    func initializeView(_placeholder: String, _delegate: UITextFieldDelegate?) {
        delegate = _delegate
        font = FontManager.font(fontName: Fonts.bold, fontSize: font?.pointSize ?? 16.0)
        backgroundColor = UIColor.cellBackgroundColor
        textColor = UIColor.textColor
        tintColor = UIColor.textColor
        placeholder = _placeholder
        attributedPlaceholder = NSAttributedString(string: placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : UIColor.textColor])
        keyboardType = .numberPad
        returnKeyType = .done
    }
}
