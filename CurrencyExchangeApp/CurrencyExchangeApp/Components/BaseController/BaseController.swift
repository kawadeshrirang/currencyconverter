//
//  BaseController.swift
//  CurrencyExchangeApp
//
//  Created by Shrirang C. Kawade on 24/03/22.
//

import UIKit

class BaseController: UIViewController {
    
    // MARK: Private MEthods
    private func addBackNavigationButton() {
        let buttonItem = UIBarButtonItem(image: UIImage(systemName: "chevron.left"), style: .plain, target: self, action: #selector(self.backButtonAction(_:)))
        buttonItem.tintColor = UIColor.buttonColor
        self.navigationItem.leftBarButtonItem = buttonItem
    }

    // MARK: LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.viewBackgroundColor
    }
    
    // MARK: Override public Methods
    public func initializeNavigation(withTitle title: String, showBackButton: Bool = true) {
        self.title = title
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: FontManager.font(fontName: Fonts.semiBold, fontSize: 25.0)]
        self.navigationItem.hidesBackButton = true
        if showBackButton {
            addBackNavigationButton()
        }
    }
    
    // MARK: Event Methods
    @objc func backButtonAction(_ sender: UIButton?) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func tapGestureAction(_ sender: UIButton?) {
        self.view.endEditing(true)
    }
    
    // MARK: Public Method
    func addTapGesture() {
        let gesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction(_:)))
        view.addGestureRecognizer(gesture)
    }
}
