//
//  CurrencyExchangeView.swift
//  CurrencyExchangeApp
//
//  Created by Shrirang C. Kawade on 25/03/22.
//

import UIKit
import NetworkCallLibrary
import CurrencyLibrary

class CurrencyExchangeViewCell: UITableViewCell {
    
    // MARK: Property
    static let identifier = "CurrencyExchangeViewIdentifier"
    @IBOutlet weak var australiaCountryTitleLabel: ThemeLabel!
    @IBOutlet weak var otherCountryTitleLabel: ThemeLabel!
    @IBOutlet weak var australiaCountryTextField: ThemeTextField!
    @IBOutlet weak var otherCountryTextField: ThemeTextField!
    var exchangeRate: ExchangeRate? = nil
    
    // MARK: Private Methods
    private func setupComponents() {
        
        // Title Labels
        australiaCountryTitleLabel.initializeView(titleText: DataValidationConstants.DEFAULT_STRING)
        otherCountryTitleLabel.initializeView(titleText: DataValidationConstants.DEFAULT_STRING)
        
        // TextFields
        australiaCountryTextField.initializeView(_placeholder: AllTextsStrings.currencyExchange.localizedString, _delegate: self)
        otherCountryTextField.initializeView(_placeholder: AllTextsStrings.currencyExchange.localizedString, _delegate: nil)
        otherCountryTextField.isUserInteractionEnabled = false
    }
    
    // MARK: LifeCycle Method
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = UIColor.clear
        setupComponents()
    }
    
    // MARK: Public Method
    func updateData(for _country: ExchangeRate) {
        exchangeRate = _country
        australiaCountryTitleLabel.text = AllTextsStrings.australia.localizedString
        otherCountryTitleLabel.text = exchangeRate?.country
    }
}

// MARK: UITextFieldDelegate
extension CurrencyExchangeViewCell: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let australiaDollerAmount: String = australiaCountryTextField.text ?? DataValidationConstants.DEFAULT_STRING
        let amount: Double = CurrencyManager.getCurrency(fromAmountString: australiaDollerAmount, currencyCode: CurrencyManager.australiaCurrencyCode).doubleValue
        let rateExchangeValue: Double = CurrencyManager.convertCurrency(australiaDollerAmount: amount, conversionRate: Double(exchangeRate?.exchangeRate.doubleValue ?? 0.0))
        otherCountryTextField.text = "\(rateExchangeValue)"
        australiaCountryTextField.text = australiaDollerAmount.isEmpty ? "\(DataValidationConstants.DEFAULT_LONG_INTEGER)" : australiaDollerAmount
    }
}
