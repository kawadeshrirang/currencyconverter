//
//  CountryTableViewCell.swift
//  CurrencyExchangeApp
//
//  Created by Shrirang C. Kawade on 23/03/22.
//

import UIKit

class CountryTableViewCell: UITableViewCell {

    // MARK: Property
    static let identifier = "CountryListingIdentifier"
    
    // MARK: LifeCycle Method
    override func updateConfiguration(using state: UICellConfigurationState) {
        super.updateConfiguration(using: state)
        guard var cConfig = contentConfiguration?.updated(for: state) as? UIListContentConfiguration else {
            contentConfiguration = defaultContentConfiguration()
            return
        }
        cConfig.textProperties.colorTransformer = UIConfigurationColorTransformer { color in
            UIColor.textColor
        }
        cConfig.secondaryTextProperties.colorTransformer = UIConfigurationColorTransformer { color in
            UIColor.textColor
        }
        cConfig.textProperties.font = FontManager.font(fontName: Fonts.semiBold, fontSize: cConfig.textProperties.font.pointSize)
        cConfig.secondaryTextProperties.font = FontManager.font(fontName: Fonts.bold, fontSize: cConfig.textProperties.font.pointSize)
        contentConfiguration = cConfig
    }
    
    // MARK: Public Method
    func updateCell(withData data: CountryDisplayData) {
        var content = defaultContentConfiguration()
        content.text = data.country
        content.secondaryText = data.currencyDisplayData
        contentConfiguration = content
        backgroundColor = UIColor.cellBackgroundColor
    }
}
