//
//  ExchangeRate.swift
//  CurrencyExchangeApp
//
//  Created by Shrirang C. Kawade on 25/03/22.
//

import Foundation

struct ExchangeRate {
    
    // MARK: Properties
    var country: String
    var exchangeRate: NSNumber
}
