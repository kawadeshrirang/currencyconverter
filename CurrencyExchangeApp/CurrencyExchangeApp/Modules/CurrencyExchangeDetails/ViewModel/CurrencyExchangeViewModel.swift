//
//  CurrencyExchangeViewModel.swift
//  CurrencyExchangeApp
//
//  Created by Shrirang C. Kawade on 25/03/22.
//

import Foundation
import UIKit

class CurrencyExchangeViewModel {
    
    // MARK: Properties
    private var country: CountryCurrency!
    
    // MARK: LifeCycle Method
    init(_country: CountryCurrency) {
        country = _country
    }
    
    // MARK: Public Method
    func screenTitle() -> String {
        return AllTextsStrings.currencyExchange.localizedString
    }
    
    func getCountryExchangeRate() -> ExchangeRate {
        return ExchangeRate(country: country.country, exchangeRate: country.buyTT)
    }
    
    func viewToDisplay() -> Int {
        return 1
    }
    
    func viewDefaultHeight() -> CGFloat {
        return 250.0
    }
}
