//
//  CurrencyExchangeDetailsController.swift
//  CurrencyExchangeApp
//
//  Created by Shrirang C. Kawade on 25/03/22.
//

import UIKit

class CurrencyExchangeDetailsController: BaseController {
    
    // MARK: Properties
    @IBOutlet weak var tableview: UITableView!
    var viewModel: CurrencyExchangeViewModel!
    
    // MARK: Private Methods
    private func initializeNavigation() {
        super.initializeNavigation(withTitle: viewModel.screenTitle())
    }
    
    private func initializeTableView() {
        tableview.backgroundColor = UIColor.clear
        tableview.register(ScreenManager.shared.xibForClass(className: CurrencyExchangeViewCell.self, owner: nil), forCellReuseIdentifier: CurrencyExchangeViewCell.identifier)
        tableview.reloadData()
    }

    // MARK: LifeCycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeNavigation()
        initializeTableView()
        addTapGesture()
    }
}

// MARK: UITableViewDelegate
extension CurrencyExchangeDetailsController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.viewDefaultHeight()
    }
}

// MARK: UITableViewDataSource
extension CurrencyExchangeDetailsController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.viewToDisplay()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CurrencyExchangeViewCell = tableView.dequeueReusableCell(withIdentifier: CurrencyExchangeViewCell.identifier, for: indexPath) as! CurrencyExchangeViewCell
        cell.updateData(for: viewModel.getCountryExchangeRate())
        return cell
    }
}
