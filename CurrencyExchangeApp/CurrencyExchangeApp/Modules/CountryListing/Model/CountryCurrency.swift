//
//  CountryCurrency.swift
//  CurrencyExchangeApp
//
//  Created by Shrirang C. Kawade on 23/03/22.
//

import Foundation
import NetworkCallLibrary

class CountryCurrency {
    
    // MARK: Properties
    var sellNotes: String
    var buyNotes: String
    var currencyName: String
    var spotRate_Date_Fmt: NSNumber
    var sellTT: NSNumber
    var buyTT: NSNumber
    var currencyCode: String
    var country: String
    var updateDateFormat: String
    var lastUpdated: String
    var effectiveDateFormat: String
    var buyTC: String
    
    // MARK: LifeCycle Methods
    init(data: [String : Any]) {
        sellNotes = DataValidationUtility.validateInstance(data["sellNotes"], dataType: .string) as! String
        buyNotes = DataValidationUtility.validateInstance(data["buyNotes"], dataType: .string) as! String
        currencyName = DataValidationUtility.validateInstance(data["currencyName"], dataType: .string) as! String
        spotRate_Date_Fmt = DataValidationUtility.validateInstance(data["SpotRate_Date_Fmt"], dataType: .long_INTEGER) as! NSNumber
        sellTT = DataValidationUtility.validateInstance(data["sellTT"], dataType: .double) as! NSNumber
        buyTT = DataValidationUtility.validateInstance(data["buyTT"], dataType: .double) as! NSNumber
        currencyCode = DataValidationUtility.validateInstance(data["currencyCode"], dataType: .string) as! String
        country = DataValidationUtility.validateInstance(data["country"], dataType: .string) as! String
        updateDateFormat = DataValidationUtility.validateInstance(data["updateDate_Fmt"], dataType: .string) as! String
        lastUpdated = DataValidationUtility.validateInstance(data["LASTUPDATED"], dataType: .string) as! String
        effectiveDateFormat = DataValidationUtility.validateInstance(data["effectiveDate_Fmt"], dataType: .string) as! String
        buyTC = DataValidationUtility.validateInstance(data["buyTC"], dataType: .string) as! String
    }
}

struct CountryDisplayData {
    
    // MARK: Properties
    var country: String
    var currencyDisplayData: String
}

