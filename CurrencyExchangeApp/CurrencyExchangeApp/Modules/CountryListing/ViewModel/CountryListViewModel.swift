//
//  CountryListViewModel.swift
//  CurrencyExchangeApp
//
//  Created by Shrirang C. Kawade on 23/03/22.
//

import Foundation
import NetworkCallLibrary
import CurrencyLibrary

class CountryListViewModel {
    
    // MARK: Properties
    private var countryList: [CountryCurrency] = []
    private let dataTransfer = CountryCurrencyDataTransfer()
    private var success: ((_ data: [CountryCurrency]) -> (Void))? = nil
    private var failuer: ((_ error: NSError?) -> (Void))? = nil
    
    // MARK: Private Methods
    private func currencyRate(amount: Double, currencyCode: String) -> String {
        return CurrencyManager.getCurrency(fromAmount: amount, currencyCode: currencyCode)
    }
    
    private func setCountryList(list: [CountryCurrency]) {
        countryList.removeAll()
        countryList = Array(list)
        success?(countryList)
    }
    
    // MARK: Public Methods
    func fetchCountries(_success: @escaping (_ data: [CountryCurrency]) -> (Void), _failuer: @escaping (_ error: NSError?) -> (Void)) {
        success = _success
        failuer = _failuer
        dataTransfer.dataTransferDelegate = self
        dataTransfer.getCountryList()
    }
    
    func screenTitle() -> String {
        return AllTextsStrings.countryListTitle.localizedString
    }
    
    func listCount() -> Int {
        return countryList.count
    }
    
    func country(at index: Int) -> CountryCurrency {
        return countryList[index]
    }
    
    func getDisplayDate(forCountry country: CountryCurrency) -> CountryDisplayData {
        let countryName: String = country.country
        var currencyText: String = ""
        if country.buyTT.doubleValue > 0 {
            let countryCurrency: String = currencyRate(amount: country.buyTT.doubleValue, currencyCode: country.currencyCode)
            let australiaCurrency: String = currencyRate(amount: CurrencyLibrary.CurrencyManager.oneAustralianDoller, currencyCode: CurrencyLibrary.CurrencyManager.australiaCurrencyCode)
            currencyText = "\(australiaCurrency) / \(countryCurrency)"
        } else {
            currencyText = "-"
        }
        
        let displayData: CountryDisplayData = CountryDisplayData(country: countryName, currencyDisplayData: currencyText)
        return displayData
    }
    
    func rowsInEachSection() -> Int {
        return 1
    }
}

// MARK: DataTransferDelegate
extension CountryListViewModel: DataTransferDelegate {
    
    func didRecieveResponse(dataTransfer: BaseDataTransfer?, data: Any?) {
        var list: [CountryCurrency] = []
        if let data = data as? [CountryCurrency] {
            list = Array(data)
        }
        setCountryList(list: list)
    }
    
    func didRecieveError(dataTransfer: BaseDataTransfer?, error: NSError?) {
        failuer?(error)
    }
}
