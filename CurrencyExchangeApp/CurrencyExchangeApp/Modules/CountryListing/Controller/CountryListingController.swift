//
//  CountryListingController.swift
//  CurrencyExchangeApp
//
//  Created by Shrirang C. Kawade on 23/03/22.
//

import UIKit

class CountryListingController: BaseController {
    
    // MARK: Properties
    @IBOutlet weak var tableview: UITableView!
    let viewModel: CountryListViewModel = CountryListViewModel()
    
    // MARK: Private Methods
    private func initializeNavigation() {
        super.initializeNavigation(withTitle: viewModel.screenTitle(), showBackButton: false)
        
        let refreshButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(self.refreshButtonAction(_:)))
        refreshButtonItem.tintColor = UIColor.buttonColor
        self.navigationItem.rightBarButtonItem = refreshButtonItem
    }
    
    private func initializeTableView() {
        tableview.backgroundColor = UIColor.clear
        tableview.register(CountryTableViewCell.self, forCellReuseIdentifier: CountryTableViewCell.identifier)
    }
    
    private func refreshList() {
        viewModel.fetchCountries { [weak self] data in
            self?.tableview.reloadData()
        } _failuer: { error in
            // handle Error workflow
            // Currently we have not added anything to this workflow.
        }
    }

    // MARK: LifeCycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeTableView()
        initializeNavigation()
        refreshList()
    }
    
    // MARK: Event Methods
    @objc func refreshButtonAction(_ sender: UIButton?) {
        refreshList()
    }
}

// MARK: UITableViewDelegate
extension CountryListingController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableview.deselectRow(at: indexPath, animated: true)
        
        let detailsController = ScreenManager.shared.getViewController(controller: CurrencyExchangeDetailsController.self) as! CurrencyExchangeDetailsController
        detailsController.viewModel = CurrencyExchangeViewModel(_country: viewModel.country(at: indexPath.section))
        ScreenManager.shared.pushController(of: detailsController, navigationController: self.navigationController)
    }
}

// MARK: UITableViewDataSource
extension CountryListingController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.listCount()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.rowsInEachSection()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CountryTableViewCell = tableView.dequeueReusableCell(withIdentifier: CountryTableViewCell.identifier, for: indexPath) as! CountryTableViewCell
        let country = viewModel.country(at: indexPath.section)
        cell.updateCell(withData: viewModel.getDisplayDate(forCountry: country))
        return cell
    }
}
