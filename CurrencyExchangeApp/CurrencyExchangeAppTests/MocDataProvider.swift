//
//  MocDataProvider.swift
//  CurrencyExchangeAppTests
//
//  Created by Shrirang C. Kawade on 26/03/22.
//

import Foundation
@testable import CurrencyExchangeApp

class MocDataProvider {
    
    static let shared: MocDataProvider = MocDataProvider()
    
    private init() {
        
    }
    
    func mocCountryCurrency() -> CountryCurrency {
        var mocResponseDisctionaty: [String : Any] = [:]
        mocResponseDisctionaty["sellNotes"] = "Testing"
        mocResponseDisctionaty["buyNotes"] = "Testing"
        mocResponseDisctionaty["currencyName"] = "US Doller"
        mocResponseDisctionaty["SpotRate_Date_Fmt"] = ""
        mocResponseDisctionaty["sellTT"] = "1.2"
        mocResponseDisctionaty["buyTT"] = "1.3"
        mocResponseDisctionaty["currencyCode"] = "USD"
        mocResponseDisctionaty["country"] = "United States"
        mocResponseDisctionaty["updateDate_Fmt"] = ""
        mocResponseDisctionaty["LASTUPDATED"] = ""
        mocResponseDisctionaty["effectiveDate_Fmt"] = ""
        mocResponseDisctionaty["buyTC"] = ""
        
        
        let countryCurrency: CountryCurrency = CountryCurrency(data: mocResponseDisctionaty)
        return countryCurrency
    }
    
    func mocDefaultText() -> String {
        return "Testing Text"
    }
}
