//
//  ThemeLabelTests.swift
//  CurrencyExchangeAppTests
//
//  Created by Shrirang C. Kawade on 27/03/22.
//

import XCTest
@testable import CurrencyExchangeApp

class ThemeLabelTests: XCTestCase {
    
    // MARK: Properties
    var mocThemeLabel: ThemeLabel!

    // MARK: LifeCycle Methods
    override func setUpWithError() throws {
        mocThemeLabel = ThemeLabel(frame: CGRect.zero)
        mocThemeLabel.initializeView(titleText: MocDataProvider.shared.mocDefaultText())
    }
    
    // MARK: Unit Tests
    func testThemeLable() {
        XCTAssertEqual(mocThemeLabel.text, MocDataProvider.shared.mocDefaultText())
        XCTAssertEqual(mocThemeLabel.font, FontManager.font(fontName: Fonts.semiBold, fontSize: mocThemeLabel.font.pointSize))
        XCTAssertEqual(mocThemeLabel.textColor, UIColor.textColor)
    }
}
