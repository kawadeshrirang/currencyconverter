//
//  ThemeTextFieldTests.swift
//  CurrencyExchangeAppTests
//
//  Created by Shrirang C. Kawade on 27/03/22.
//

import XCTest
@testable import CurrencyExchangeApp

class ThemeTextFieldTests: XCTestCase {
    
    // MARK: Properties
    var mocThemeTextField: ThemeTextField!

    // MARK: LifeCycle Methods
    override func setUpWithError() throws {
        mocThemeTextField = ThemeTextField(frame: CGRect.zero)
        mocThemeTextField.initializeView(_placeholder: MocDataProvider.shared.mocDefaultText(), _delegate: nil)
    }
    
    // MARK: Unit Tests
    func testThemeTextField() {
        XCTAssertNil(mocThemeTextField.delegate)
        XCTAssertEqual(mocThemeTextField.font, FontManager.font(fontName: Fonts.bold, fontSize: mocThemeTextField.font?.pointSize ?? 16.0))
        XCTAssertEqual(mocThemeTextField.backgroundColor, UIColor.cellBackgroundColor)
        XCTAssertEqual(mocThemeTextField.textColor, UIColor.textColor)
        XCTAssertEqual(mocThemeTextField.tintColor, UIColor.textColor)
        XCTAssertEqual(mocThemeTextField.placeholder, MocDataProvider.shared.mocDefaultText())
        XCTAssertEqual(mocThemeTextField.keyboardType, .numberPad)
        XCTAssertEqual(mocThemeTextField.returnKeyType, .done)
    }
}
