//
//  BaseControllerTests.swift
//  CurrencyExchangeAppTests
//
//  Created by Shrirang C. Kawade on 27/03/22.
//

import XCTest
@testable import CurrencyExchangeApp

class BaseControllerTests: XCTestCase {

    // MARK: Properties
    var mocBaseController: BaseController!
    
    // MARK: Lifecycle Methods
    override func setUpWithError() throws {
        mocBaseController = BaseController()
    }
    
    // MARK: Unit Tests
    func testViewDidLoad() {
        mocBaseController.viewDidLoad()
        mocBaseController.addTapGesture()
        XCTAssertEqual(mocBaseController.view.backgroundColor, UIColor.viewBackgroundColor)
        XCTAssertFalse(mocBaseController.view.gestureRecognizers?.isEmpty ?? true)
    }
    
    func testInitializeNavigationWithBackButton() {
        mocBaseController.initializeNavigation(withTitle: MocDataProvider.shared.mocDefaultText(), showBackButton: true)
        XCTAssertEqual(mocBaseController.title, MocDataProvider.shared.mocDefaultText())
        XCTAssertTrue(mocBaseController.navigationItem.hidesBackButton)
        XCTAssertNotNil(mocBaseController.navigationItem.leftBarButtonItem)
    }
    
    func testInitializeNavigationWithoutBackButton() {
        mocBaseController.initializeNavigation(withTitle: MocDataProvider.shared.mocDefaultText(), showBackButton: false)
        XCTAssertEqual(mocBaseController.title, MocDataProvider.shared.mocDefaultText())
        XCTAssertTrue(mocBaseController.navigationItem.hidesBackButton)
        XCTAssertNil(mocBaseController.navigationItem.leftBarButtonItem)
    }
}
