//
//  CountryListViewModelTests.swift
//  CurrencyExchangeAppTests
//
//  Created by Shrirang C. Kawade on 26/03/22.
//

import XCTest
@testable import CurrencyExchangeApp

class CountryListViewModelTests: XCTestCase {

    // MARK: Properties
    private var mocViewModel: CountryListViewModel!
    private var listResult: Bool = false
    
    // MARK: LifeCycle Method
    override func setUpWithError() throws {
        mocViewModel = CountryListViewModel()
    }
    
    // MARK: Unit Tests
    func testFetchCountries() {
        let expectation1 = expectation(description: "fetchList")
        mocViewModel.fetchCountries { [weak self] data in
            let result: CountryCurrency? = self?.mocViewModel.country(at: 0)
            XCTAssertNotNil(result)
            
            let result1: Int? = self?.mocViewModel.listCount()
            XCTAssertNotEqual(result1, 0)
            
            let country: CountryCurrency = (self?.mocViewModel.country(at: 0))!
            let result2: CountryDisplayData = (self?.mocViewModel.getDisplayDate(forCountry: country))!
            XCTAssertEqual(result2.country, country.country)
            XCTAssertNotEqual(result2.currencyDisplayData, "")
            
            expectation1.fulfill()
        } _failuer: { [weak self] error in
            let result: Int? = self?.mocViewModel.listCount()
            XCTAssertEqual(result, 0)
            expectation1.fulfill()
        }

        self.wait(for: [expectation1], timeout: 30)
    }
    
    func testScreenTitle() {
        let result: String = mocViewModel.screenTitle()
        XCTAssertEqual(result, AllTextsStrings.countryListTitle.localizedString)
    }
    
    func testRowsInEachSection() {
        let result: Int = mocViewModel.rowsInEachSection()
        XCTAssertEqual(result, 1)
    }
}
