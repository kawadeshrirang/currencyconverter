//
//  CurrencyExchangeViewModelTests.swift
//  CurrencyExchangeAppTests
//
//  Created by Shrirang C. Kawade on 26/03/22.
//

import XCTest
@testable import CurrencyExchangeApp

class CurrencyExchangeViewModelTests: XCTestCase {
    
    // MARK: Properties
    private var mocViewModel: CurrencyExchangeViewModel!
    private var mocCountryObject: CountryCurrency!

    // MARK: LifeCycle Method
    override func setUpWithError() throws {
        mocCountryObject = MocDataProvider.shared.mocCountryCurrency()
        mocViewModel = CurrencyExchangeViewModel(_country: mocCountryObject)
    }
    
    // MARK: Unit Tests
    func testScreenTitle() {
        let result: String = mocViewModel.screenTitle()
        XCTAssertEqual(result, AllTextsStrings.currencyExchange.localizedString)
    }
    
    func testGetCountryExchangeRate() {
        let result: ExchangeRate = mocViewModel.getCountryExchangeRate()
        XCTAssertEqual(result.country, mocCountryObject.country)
        XCTAssertEqual(result.exchangeRate, mocCountryObject.buyTT)
    }
    
    func testViewToDisplay() {
        let result: Int = mocViewModel.viewToDisplay()
        XCTAssertEqual(result, 1)
    }
    
    func testViewDefaultHeight() {
        let result: CGFloat = mocViewModel.viewDefaultHeight()
        XCTAssertEqual(result, 250.0)
    }
}
