#NetworkCallLibrary

As part of API calling within application, we have created this static library.

##Overview

- We can use this static library for use of API call communication. We have handled API call comminication workflows.
- We can use this same library in several project as a resubale code library.
- If any new requirement comes then we can add those functionalities into library codebase and we can provide to end projects as updated library file to new funcionality.
- With this approach we can keep cental code in form of static libraries or framerowks.
