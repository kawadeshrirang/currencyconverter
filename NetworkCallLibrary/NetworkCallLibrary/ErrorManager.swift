//
//  ErrorManager.swift
//  NetworkCallLibrary
//
//  Created by Shrirang C. Kawade on 23/03/22.
//

import Foundation

// Error Manager is written to validate/seperate the inital error categarisation.
// Based on error we can manage the special error workflows.
// This is just for example that we can do someting like this to follow the special case handleing of error with error type/code.
/// This class this Singleton class. It provided to manage Error manupulation or to handle error related workflows.
///
/// ```
/// ```
///
open class ErrorManager {
    
    //MARK: Properties
    /// 'shared' Singleton instance of ErrorManager class.
    ///
    /// ```
    /// ```
    ///
    public static let shared: ErrorManager = ErrorManager()
    
    //MARK: LifeCycle Method
    /// Private initialization method to avoid seperate initialization of object.
    ///
    /// ```
    /// ```
    private init() {
        // To avoid the seperate initialisation of this class.
        // Bacause we are using this class as Singleton class.
    }
    
    //MARK: Public Method
    /// This function check specific error codes and throws the expection with APIErrors enum value.
    ///
    /// ```
    /// The NSLocal object will get with "en_US" identifier.
    /// ```
    ///
    /// - Warning:
    /// - Parameter responseCode: The API response code.
    /// - Returns: Throws expection with APIErrors enum.
    open func checkError(responseCode: Int?) throws {
        guard let responseCode = responseCode else {
            throw APIErrors.unknownError
        }

        let errorCode: APIErrorCodes? = APIErrorCodes(rawValue: responseCode)
        switch errorCode {
            case .scheduledMaintenance:
                throw APIErrors.scheduledMaintenance
            case .noNetwork:
                throw APIErrors.noNetwork
            case .unknownError:
                throw APIErrors.unknownError
            default:
                throw APIErrors.unknownError
        }
    }
}
