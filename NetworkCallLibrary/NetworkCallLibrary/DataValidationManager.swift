//
//  DataValidationManager.swift
//  NetworkCallLibrary
//
//  Created by Shrirang C. Kawade on 23/03/22.
//

import Foundation

open class DataValidationConstants {
    
    public static let DEFAULT_STRING = ""
    public static let DEFAULT_LONG_INTEGER = NSNumber(value: 0)
    public static let DEFAULT_DOUBLE = NSNumber(value: 0.0)
    public static let DEFAULT_FALSE_BOOLEN = false
    public static let DEFAULT_TRUE_BOOLEN = true
    public static let DEFAULT_ARRAY = [] as [Any]
    public static let DEFAULT_DICTIONARY = [:] as [String:Any]
    
    public enum DataTypeEnum : Int {
        
        case string = 0
        case double = 2
        case false_boolen = 3
        case long_INTEGER = 4
        case array = 5
        case dictionary = 6
        case true_boolen = 7
    }
}

open class DataValidationUtility {
    
    //MARK: Validation Methods
    
    // This method will validate instance object.
    // If instance object is NULL, then it returns nil value
    open class func validateNull(for instance: Any?)-> Any? {
        return instance is NSNull ? nil : instance
    }
    
    // This method will validate instance object.
    // If instance object is not valid, then it returns default value of given dataype
    open class func validateInstance(_ instance: Any?, dataType: DataValidationConstants.DataTypeEnum) -> Any {
        
        if let object = DataValidationUtility.validateNull(for: instance) {
            switch dataType {
                case .string: do {
                        if object is String {
                            let text = (object as! String)
                            return DataValidationUtility.validateStringByRemovingUnwantedWords(text)
                        } else if object is NSNumber {
                            let text = (object as! NSNumber).stringValue
                            return DataValidationUtility.validateStringByRemovingUnwantedWords(text)
                        } else {
                            return DataValidationConstants.DEFAULT_STRING
                        }
                }
                case .long_INTEGER: do {
                    if object is NSNumber {
                        return object
                    } else if object is String {
                        let text = validateStringByRemovingUnwantedWords(object as! String)
                        let finalValue: NSNumber = convertTextToNumber(text: text)
                        return finalValue
                    } else {
                        return DataValidationConstants.DEFAULT_LONG_INTEGER
                    }
                }
                case .double: do {
                    if object is NSNumber {
                        return object
                    } else if object is String {
                        let text = validateStringByRemovingUnwantedWords(object as! String)
                        let finalValue: Any = convertTextToNumber(text: text)
                        return finalValue
                    } else {
                        return DataValidationConstants.DEFAULT_DOUBLE
                    }
                }
                case .false_boolen, .true_boolen: do {
                    if object is NSNumber {
                        return (object as! NSNumber).boolValue
                    } else {
                        return dataType == .false_boolen ? DataValidationConstants.DEFAULT_FALSE_BOOLEN : DataValidationConstants.DEFAULT_TRUE_BOOLEN
                    }
                }
                case .dictionary: do {
                    if object is [String:Any] {
                        return object
                    } else {
                        return DataValidationConstants.DEFAULT_DICTIONARY
                    }
                }
                case .array: do {
                    if object is [Any] {
                        return object
                    } else {
                        return DataValidationConstants.DEFAULT_ARRAY
                    }
                }
            }
            
        } else {
            var finalInstenceValue: Any? = nil
            
            switch dataType {
            case .string:
                finalInstenceValue = DataValidationConstants.DEFAULT_STRING
                
            case .false_boolen:
                finalInstenceValue = DataValidationConstants.DEFAULT_FALSE_BOOLEN
                
            case .true_boolen:
                finalInstenceValue = DataValidationConstants.DEFAULT_TRUE_BOOLEN
                
            case .double:
                finalInstenceValue = DataValidationConstants.DEFAULT_DOUBLE
                
            case .long_INTEGER:
                finalInstenceValue = DataValidationConstants.DEFAULT_LONG_INTEGER
                
            case .array:
                finalInstenceValue = DataValidationConstants.DEFAULT_ARRAY
                
            case .dictionary:
                finalInstenceValue = DataValidationConstants.DEFAULT_DICTIONARY
            }
            
            return finalInstenceValue!
        }
    }
    
    // This method will validate String instance object.
    // If String instance object is invalid words, then it returns Blank Text
    open class func validateStringByRemovingUnwantedWords(_ text: String) -> String {
        let finalText = text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if finalText.lowercased() == "(null)" || finalText.lowercased() == "null" || finalText.lowercased() == "<null>" || finalText.lowercased() == "{null}" || finalText.lowercased() == "," {
            return DataValidationConstants.DEFAULT_STRING as String
            
        } else {
            return finalText
        }
    }
    
    open class func convertTextToNumber(text: String) -> NSNumber {
        let formatter = NumberFormatter()
        formatter.locale = NSLocale(localeIdentifier: "en_US") as Locale
        formatter.numberStyle = .decimal
        return formatter.number(from: text) ?? DataValidationConstants.DEFAULT_DOUBLE
    }
}
