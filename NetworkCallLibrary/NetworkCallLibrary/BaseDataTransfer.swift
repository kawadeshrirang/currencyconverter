//
//  BaseDataTransfer.swift
//  NetworkCallLibrary
//
//  Created by Shrirang C. Kawade on 23/03/22.
//

import Foundation

// This protocol to handle workflow of Scheduled Maintenance Error if requires.
// This is depends on situation where if specific API call requirement is to handle this type of situation workflow.
// So consider this is an example.
// By seperate out the behavior we can use protocols to achieve it.
public protocol ScheduledMaintenanceErrorDelegate {
    func scheduledMaintenanceError(dataTransfer :BaseDataTransfer?, error : NSError?)
}

// This protocol to handle workflow of No Network Error if requires.
// This is depends on situation where if specific API call requirement is to handle this type of situation workflow.
// So consider this is an example.
// By seperate out the behavior we can use protocols to achieve it.
public protocol NoNetworkErrorDelegate {
    func noNetworkError(dataTransfer :BaseDataTransfer?, error : NSError?)
}

// This protocol to handle workflow of api callback to handle response or error.
public protocol DataTransferDelegate {
    func didRecieveResponse(dataTransfer :BaseDataTransfer?, data :Any?)
    func didRecieveError(dataTransfer :BaseDataTransfer?, error : NSError?)
}

// We can consider this as base call to API calls.
// Each API call class will require to inherite from this.
// So that class will get all default behaviour that is require to make API call as well as to handle generic workflow handling for response or error.
open class BaseDataTransfer {
    
    //MARK: Properties
    private var baseURL: String!
    public var scheduledMaintenanceErrorDelegate: ScheduledMaintenanceErrorDelegate?
    public var noNetworkErrorDelegate: NoNetworkErrorDelegate?
    public var dataTransferDelegate: DataTransferDelegate?
    
    //MARK: Private Methods
    private func prepareURLRequest(ofType URLRequestType: URLRequests) -> URLRequest? {
        var urlRequest: URLRequest? = nil
        if let url = URL(string: baseURL) {
            urlRequest = URLRequest(url: url)
            urlRequest?.httpMethod = URLRequestType.rawValue
            urlRequest?.httpBody = nil
        }
        return urlRequest
    }
    
    private func handleError(error: NSError?) {
        do {
            try ErrorManager.shared.checkError(responseCode: error?.code)
        } catch APIErrors.scheduledMaintenance {
            if scheduledMaintenanceErrorDelegate != nil {
                scheduledMaintenanceErrorDelegate?.scheduledMaintenanceError(dataTransfer: self, error: error)
            } else {
                parseErrorData(error: error)
            }
        } catch APIErrors.noNetwork {
            if noNetworkErrorDelegate != nil {
                noNetworkErrorDelegate?.noNetworkError(dataTransfer: self, error: error)
            } else {
                parseErrorData(error: error)
            }
        } catch _ {
            parseErrorData(error: error)
        }
    }
    
    private func handleResponse(data: Data) {
        let responseObject: Any? = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
        parseResposeData(data: responseObject)
    }
    
    //MARK: LifeCycle Methods
    public init(_baseURL: String) {
        baseURL = _baseURL
    }
    
    //MARK: Public Methods
    /// Make API call with provided API request Type.
    /// Example: URLRequests.get that is "GET" request type.
    ///
    /// ```
    /// ```
    ///
    /// - Parameter URLRequestType: The API request type.
    open func callWebService(ofType URLRequestType: URLRequests) {
        
        let operationBlock = BlockOperation()
        operationBlock.qualityOfService = .userInitiated
        
        operationBlock.addExecutionBlock { [weak self] in
            if let request = self?.prepareURLRequest(ofType: URLRequestType) {
                let session = URLSession(configuration: URLSessionConfiguration.default)
                    session.dataTask(with: request) { data, urlResponse, error in
                        if let error = error as NSError? {
                            self?.handleError(error: error)
                        } else if let responseData = data {
                            self?.handleResponse(data: responseData)
                        } else {
                            self?.handleError(error: nil)
                        }
                        
                    }.resume()
            } else {
                self?.handleError(error: nil)
            }
        }
        
        let operationQueue = OperationQueue()
        operationQueue.addOperation(operationBlock)
    }
    
    /// Override this method into subclass to handle response data handling.
    ///
    /// ```
    /// ```
    ///
    /// - Parameter data: The API response data object.
    open func parseResposeData(data: Any?) {
        // Override this method
    }
    
    /// Override this method into subclass to handle response error handling.
    ///
    /// ```
    /// ```
    ///
    /// - Parameter error: The API error response data object (NSError).
    open func parseErrorData(error: NSError?) {
        // Override this method
        dataTransferDelegate?.didRecieveError(dataTransfer: self, error: error)
    }
}
