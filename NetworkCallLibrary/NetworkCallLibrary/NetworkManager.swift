//
//  NetworkManager.swift
//  NetworkCallLibrary
//
//  Created by Shrirang C. Kawade on 23/03/22.
//

import Foundation

/// enum with customised error codes added by adopting Error protocol.
///
/// ```
/// case scheduledMaintenance
/// case noNetwork
/// case unknownError
/// ```
///
public enum APIErrors: Error {
    case scheduledMaintenance
    case noNetwork
    case unknownError
}

/// Int enum with Error codes.
///
/// ```
/// case scheduledMaintenance
/// case noNetwork
/// case unknownError
/// ```
///
public enum APIErrorCodes: Int {
    case scheduledMaintenance = 503
    case noNetwork = -1009
    case unknownError = -111
}

// Created the Custom Error struct. So we can consume this within application.
// We can extend the functionality of this struct as per the application need.
/// Custom Error struct. So we can consume this within application.
///
public struct APIError: LocalizedError {
    
    //MARK: Properties
    public var errorDescription: String?
    public var code: Int
    public var localizedDescription: String
    public var userInfo: Any?
    
    //MARK: LifeCycle Methods
    public init(code: Int, errorDescription: String, localizedDescription: String, userInfo: Any? = nil) {
        self.code = code
        self.errorDescription = errorDescription
        self.localizedDescription = localizedDescription
        self.userInfo = userInfo
    }
    
    //MARK: Public Methods
    /// This method create error object with generic error message.
    ///
    /// ```
    /// ```
    ///
    /// - Returns: Generic error object of APIError.
    public static func unknownError() -> APIError {
        return APIError(code: APIErrorCodes.unknownError.rawValue, errorDescription: "Unknown error occured.", localizedDescription: "Unknown error occured.")
    }
}

/// The URL request methods string enum.
///
/// ```
/// case get = "GET"
/// case post = "POST"
/// ```
///
public enum URLRequests: String {
    case get = "GET"
    case post = "POST"
}
