//
//  CurrenyManager.swift
//  CurrencyLibrary
//
//  Created by Shrirang C. Kawade on 23/03/22.
//

import Foundation

open class CurrencyManager {
    
    //MARK: Properties
    private static let defaultCurrencyCode: String = "USD"
    private static let defaultLocaleIdentifier: String = "en_US"
    /// This property hold default value as "AUD".
    ///
    public static let australiaCurrencyCode: String = "AUD"
    /// This property hold default value as 1.0.
    ///
    public static let oneAustralianDoller: Double = 1.0
    
    //MARK: Private Methods
    /// This function returns a NSLocale object for a given `localeIdentifier`.
    ///
    /// ```
    /// The NSLocal object will get with "en_US" identifier.
    /// ```
    ///
    /// - Warning:
    /// - Parameter localeIdentifier: The country code.
    /// - Returns: Locale object of  `localeIdentifier`.
    private static func getLocale(localeIdentifier: String) -> NSLocale {
        return NSLocale(localeIdentifier: localeIdentifier)
    }
    
    /// This function returns a NumberFormatter object for a given `style, currencyCode, localeIdentifier`.
    ///
    /// ```
    ///  If NumberFormatter.Style == .currency then only it assigns 'currencyCode' value.
    /// ```
    ///
    /// - Warning:
    /// - Parameter style: The number formatter style.
    /// - Parameter currencyCode: The currency code for which formatter format the amount.
    /// - Parameter localeIdentifier: The country code.
    /// - Returns: NumberFormatter object.
    private static func getCurrencyNumberFormatter(ofNumberStyle style: NumberFormatter.Style, currencyCode: String?, localeIdentifier: String) -> NumberFormatter {
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = getLocale(localeIdentifier: localeIdentifier) as Locale
        if style == .currency {
            numberFormatter.currencyCode = currencyCode ?? defaultCurrencyCode
            numberFormatter.usesGroupingSeparator = true
        }
        numberFormatter.numberStyle = style
        return numberFormatter
    }
    
    //MARK: LifeCycle Method
    /// Private Init method to avoid initialization of this class.
    ///
    /// ```
    ///
    /// ```
    ///
    private init() {
        
    }
    
    //MARK: Public Methods
    /// This function returns currency number form amount text for specific currency code that provided in 'currencyCode' value.
    ///
    /// ```
    ///  The number style decides, if currencyCode == nil then '.currency' else '.decimal'.
    /// ```
    ///
    /// - Parameter amountString: The currency amount text.
    /// - Parameter currencyCode: The currency code for which formatter format the amount.
    /// - Returns: This will returns the currency number as NSNumber.
    public static func getCurrency(fromAmountString amountString: String, currencyCode: String?) -> NSNumber {
        let numberFormatter: NumberFormatter = getCurrencyNumberFormatter(ofNumberStyle: currencyCode == nil ? .currency : .decimal, currencyCode: currencyCode, localeIdentifier: defaultLocaleIdentifier)
        return numberFormatter.number(from: amountString) ?? NSNumber(value: 0.0)
    }
    
    /// This function returns currency text form amount value for specific currency code that provided in 'currencyCode' value.
    ///
    /// ```
    ///  The number style always '.currency'.
    /// ```
    ///
    /// - Parameter amount: The currency amount.
    /// - Parameter currencyCode: The currency code for which formatter format the amount.
    /// - Returns: This will returns the currency text.
    public static func getCurrency(fromAmount amount: Double, currencyCode: String) -> String {
        let numberFormatter: NumberFormatter = getCurrencyNumberFormatter(ofNumberStyle: .currency, currencyCode: currencyCode, localeIdentifier: defaultLocaleIdentifier)
        return numberFormatter.string(from: NSNumber(value: amount)) ?? ""
    }
    
    /// This function makes currency conversion for specific currency rate for given Australia doller amount.
    ///
    /// ```
    ///
    /// ```
    ///
    /// - Parameter australiaDollerAmount: The Australia doller amount.
    /// - Parameter conversionRate: The conversion rate of specific country amount.
    /// - Returns: This will returns the calculated currency amount as per the specified Australia doller amount.
    public static func convertCurrency(australiaDollerAmount: Double, conversionRate: Double) -> Double {
        let calculatedAmount: Double = (australiaDollerAmount * conversionRate)
        return calculatedAmount
    }
}
